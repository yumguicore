# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

import sys
import logging
import gtk
import gtk.glade

import time

from yum.Errors import YumBaseError
from yum.rpmtrans import RPMTransaction


from yumguicore import YumGuiBase,YumGuiPackageQueue, YumGuiDownloadError,\
                       YumGuiDepsolveError,YumGuiDepsolveError, YumGuiTransactionError 
                       
from yumguicore.gui import UI,Controller,TextViewConsole,\
                           TextViewLogHandler,doLoggerSetup,doGtkEvents,Progress
                           
from yumguicore.views import YumGuiPackageView, YumGuiTransactionView,YumGuiGroupView                          
from yumguicore.callback import YumGuiDownloadCallback,YumGuiRPMCallback
from yumguicore.dialogs import questionDialog
import tests


class MyYumGuiApp(Controller):
    ''' This class contains all glade signal callbacks '''
    
    
    def __init__( self ):
        # Create and ui object contains the widgets.
        ui = UI( 'testgui2.glade' , 'main', 'yumex' )
        # init the Controller Class to connect signals.
        Controller.__init__( self, ui )
        self.logger = logging.getLogger('yum.verbose.MyYumGuiApp')
        self.firstRun = True
        self.packages = None
        self.main() 
         
    #****************** Glade callback methods ********************
    def onQuit(self,widget,data=None):
        '''
        Quit the application
        '''
        gtk.main_quit()       # Exit gtk
        sys.exit(0)

    def on_gotoPage1_clicked(self,widget,data=None):
        '''
        Quit the application
        '''
        self.ui.notebook.set_current_page(1)

    def on_gotoPage0_clicked(self,widget,data=None):
        '''
        Quit the application
        '''
        self.ui.notebook.set_current_page(0)
        
    
    #**************************************************************
    def setupGUI(self):
        ''' Setup the GUI & logging '''
        self.ui.main.connect( "delete_event", self.onQuit )      
        self.ui.notebook.set_show_tabs( False )
        self.ui.main.present()
        self.progress = Progress(self.ui.progressbar,self.ui.progresslabel,self.ui.vboxProgress)
        self.ui.TransactionLabel.set_markup('<big><b>Current Transaction</b></big>')
        self.rpmCallback = YumGuiRPMCallback(self.progress)


    def processTransaction(self):    
        '''
        Perform the yum transaction
         - Resolve Dependencies
         - Download Packages.
         - Check GPG Signature.
         - Run Test Transaction
         - Run Transaction
        '''
        try:
            self.progress.show()
            self.logger.info('Resolve Dependencies')
            self.ui.book.set_current_page(OUTPUT_VIEW)
            self.yumbase.doDepSolve()
            self.ui.book.set_current_page(TRANSACTION_VIEW)
            self.transView.refresh()
            self.logger.info(self.queue.list())
            if not questionDialog(self.ui.main,'Do you want to continue with the transaction'):
                return
            self.ui.book.set_current_page(OUTPUT_VIEW)
            self.logger.info('Download Packages and check signatures')
            self.yumbase.downloadPackages()
            self.logger.info('Run Test Transaction')
            self.yumbase.testTransaction()
            self.logger.info('Run Transaction')
            self.yumbase.runTransaction(self.rpmCallback)
        # Catch depsolve errors
        except YumGuiDepsolveError, msgs:
            mstrs = '\n'.join(msgs)
            msg = _( "Error in Dependency Resolution" )
            self.logger.error(msg)
            self.logger.error(mstrs)
        # catch download errors
        except YumGuiDownloadError, msgs:
            mstrs = '\n'.join(msgs)
            msg = _( "Error in Download" )
            self.logger.error(msg)
            self.logger.error(mstrs)
        # catch Transaction errors    
        except YumGuiTransactionError, msgs:
            mstrs = '\n'.join(msgs)
            self.logger.error(mstrs)
        # catch other yum errors.
        except YumBaseError, msgs:
            mstrs = '\n'.join(msgs)
            self.logger.error(mstrs)
        self.progress.hide()
    
    def setupYum(self):
        self.ui.notebook.set_current_page(1)
        self.progress.show()
        if not self.firstRun: # Dont do reset the first time
            self.yumbase.reset()
        else:
            self.firstRun = False
        self.yumbase.setup(doUpdates=True, doUpdateMetaData=False)
        # Get a list of all packages (installed + available)
        pkgs = self.yumbase.getPackages(sort=True)
        # Get a list of all packages (installed + available)
        updates = self.yumbase.getPackages(pkgTypes=['updates'],sort=True)
        # show the packages in the package view, use po.name for typeahead search
        #self.pkgView.populate(updates,'name')
        self.packages = pkgs
        self.ui.notebook.set_current_page(0)
        self.progress.hide()

    def doTests(self):
        '''
        Do some testing. 
        '''
        #tests.testProgress(self.progress)
        #tests.testTextViewConsole(self.output)
        #tests.testRPMCallback(self.rpmCallback)
        #time.sleep(5)
         
    def main(self):
        self.setupGUI()
        self.yumbase = YumGuiBase(debuglevel= 4)
        # Setup a PackageQueue an interface to yumbase.tsInfo
        self.queue = YumGuiPackageQueue(self.yumbase,debug=False)
        # Setup a PackageView to display packages.
        dlCb = YumGuiDownloadCallback(self.progress)
        self.yumbase.setDownloadCallback(dlCb)
        # setup yum (load repo metadata etc)
        self.setupYum()
        
if __name__ == "__main__":
        mainApp = MyYumGuiApp()
        gtk.main()
        
