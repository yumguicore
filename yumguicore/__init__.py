# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.


# basic Python imports
import logging

# yum imports
import yum
from yum.Errors import YumBaseError
from yum.update_md import UpdateMetadata
from yum.misc import sortPkgObj
from yum.rpmtrans import RPMTransaction


# YumGuiCore imports
from i18n import _
from misc import format_number
from callback import DepSolveCallback,YumGuiNoOutputCallback
from dialogs import questionDialog

class YumGuiError(YumBaseError):
    ''' The basic YumGuiError class '''
    def __init__(self, value=None):
        YumBaseError.__init__(self)
        self.value = value
        
YumGuiDownloadError = YumGuiDepsolveError = YumGuiTransactionError = YumGuiError

class YumGuiBase(yum.YumBase):
    """ GUI Extention to the YumBase class"""
    
    def __init__(self,configfn = "/etc/yum.conf", rootdir="/", plugins=True, parser=None,
                 debuglevel=None, errorlevel=None,disabled_plugins=None):
        '''
        @param configfn: The yum config file to use (default = '/etc/yum.conf')
        @param rootdir: The root dir to use (default = '/')
        @param plugins: Enable TYPE_CORE plugins (default = True)
        @param optparser: option parser to use.
        @param debuglevel: Debug level to use for logging. If None, the debug
            level will be read from the configuration file.
        @param errorlevel: Error level to use for logging. If None, the debug
            level will be read from the configuration file.
        @param disabled_plugins: Plugins to be disabled (default = None)          
        '''        
        yum.YumBase.__init__(self)
        # Setup the Python logger
        self._logger = logging.getLogger("yum.verbose.YumGuiBase")
        
        # Setup vars.
        
        updatesMetaData = None 
        self._dnlCallback = None # Download Callback Handler
        
        # Setup yum config
        try:
            self._getConfig(fn=configfn, root=rootdir,
                            init_plugins=plugins, plugin_types=(yum.plugins.TYPE_CORE,),
                            optparser=parser, 
                            debuglevel=debuglevel, errorlevel=errorlevel,
                            disabled_plugins=disabled_plugins)
        except yum.Errors.ConfigError, e:
            raise YumGuiError, e

        
        
    def _log(self,msg):
        ''' 
        Write message to logger 
        @param msg: message to write to logger
        '''
        self._logger.log(yum.logginglevels.INFO_2,msg)
    
    def actionLog(self,msg):
        ''' 
        action log class
        @param msg: message to to decribe the action
        write the message to the logger by default, can be overloaded by a child class to
        write the message somewhere else and increment a progress bar etc.
        '''
        self._log(msg)
        
        
    def setup(self,doUpdates=False, doUpdateMetaData = False): 
        ''' 
        Setup rpmdb, TransactionSets, Repositories, Package Sacks, Groups & Updates.
        @param doUpdates: Do updates setup (default = False)
        @param doUpdateMetaData: Setup and grab extra updates metadata (Default = False) 
        '''
        self.actionLog( _( "Setup Transaction Sets" ) )
        self.doTsSetup()
        self.actionLog( _( "Setup RPM Db" ) )
        self.doRpmDBSetup()
        self.actionLog( _( "Setup Repositories" ) )
        self.doRepoSetup()
        self.actionLog( _( "Setup Package Sacks" ) )
        self.doSackSetup()
        if doUpdates: 
            self.actionLog( _( "Setup Updates" ) )
            self.doUpdateSetup()
            if doUpdateMetaData:
                self._setupUpdateMetadata()        
        self.actionLog( _( "Setup Groups" ) )
        self.doGroupSetup()      
        self.actionLog( _( "Setup Completed" ) )
        
    def reset(self):
        '''
        Reset yum, so setup can be run again
        '''
        self._repos = yum.repos.RepoStorage(self)   
        self.getReposFromConfig()

        # Kill the current pkgSack    
        if self.pkgSack:
            self.pkgSack=None

        self.closeRpmDB()

    def resetTs(self):
        '''Clear the current tsInfo Transaction Set'''
        # clear current tsInfo, we want a empty one.
        del self.tsInfo
        self.tsInfo = self._transactionDataFactory()
        self.initActionTs()
        
        
    def _setupUpdateMetadata(self):
        ''' Setup & grap the available updates metadata '''
        self.updateMetadata = UpdateMetadata()        
        for repo in self.repos.listEnabled():
            try: # attempt to grab the updateinfo.xml.gz from the repodata
                self.updateMetadata.add(repo)
                self._log(_("Loaded update Metadata from %s ") % repo.id)
            except yum.Errors.RepoMDError:
                self._log(_("No update Metadata found for %s ") % repo.id)
        
    def setDownloadCallback(self,callback):
        '''
        Set the callback handler to use while downloading.
        @param callback: the download callback class.
        '''                
        self.repos.setProgressBar( callback )
        self._dnlCallback = callback
        
    def setCacheCallback(self,callback):
        '''
        Set the callback handler to use while populating the sqlite cache.
        @param callback: the download callback class.
        '''                
        self.repos.callback = callback
              

    def doDepSolve(self,callback=None):
        '''
        Resolve the dependencies in current transaction
        @param callback: The depsolve callback class to use. 
        @return: (result, msgs) from  YumBase.buildTransaction()
        '''
        if callback:
            self.dsCallback = callback
        elif not self.dsCallback:
            self.dsCallback = DepSolveCallback() 
        try:
            rc,msgs =  self.buildTransaction() 
            if rc != 2: # Something went wrong in the depsolve
                raise YumGuiDepsolveError, msgs
            return 
        except yum.Errors.YumBaseError, e:
            raise YumGuiDepsolveError, [str( e )]

        
    def _askForGPGKeyImport(self, po, userid, hexkeyid):
        ''' ask callback for GPG Key import '''
        #print "po: %s userid: %s hexkey: %s " % (str(po),userid,hexkeyid)
        msg =  _('Do you want to import GPG Key : %s \n') % hexkeyid 
        msg += "  %s \n" % userid
        msg += _("Needed by %s") % str(po)
        return questionDialog(self.mainwin, msg)
    
    def simpleDBInstalled(self, name, arch=None):
        '''
        Fast way to find out id name.arch is installed
        @param name: Name of the package
        @param arch: Arch of the package
        '''
        # FIXME: doing this directly instead of using self.rpmdb.installed()
        # speeds things up by 400%
        mi = self.ts.ts.dbMatch('name', name)
        if mi.count() == 0:
            return False
        if arch is None:
            return True
        if arch in map(lambda h: h['arch'], mi):
            return True
        return False


    def isGroupInstalled(self, grp):
        '''
        Check if group is install
        @param grp: Group to check
        '''
        if grp.selected:
            return True
        elif grp.installed and not grp.toremove:
            return True
        if (len(grp.optional_packages.keys()) ==
            len(grp.mandatory_packages.keys()) ==
            len(grp.default_packages.keys()) == 0) and \
            len(grp.conditional_packages.keys()) != 0:
            return True
        return False

    def deselectGroup(self, grpid):
        '''
        Remove the packages in the group from the transaction.
        if the group is installed on the system the add remove actions
        for all the packages in the group to the transaction.
        '''
        group = self.comps.return_group(grpid)
        if group.selected:
            yum.YumBase.deselectGroup(self, grpid)
        else:
            self.groupRemove(grpid)

    def selectGroup(self, grpid):
        '''
        Add the packages in the group to the transaction, if group is allready added to
        the transaction for removal, the revert the removal.
        '''
        group = self.comps.return_group(grpid)
        if group.toremove:
            self.groupUnremove(grpid)
        else:
            yum.YumBase.selectGroup(self, grpid)
            
    def getPackages(self,pkgTypes = ['all'],sort=False):
        '''
        Return list of yum package objects.
        @param pkgTypes: Types of packages to return (['all','installed','available','updates'])
        @param sort: Sort the package list (Default = False)
        '''
        if 'all' in pkgTypes:
            pkgTypes = ['installed','available']
        pkgs = []
        # Get the installed packages
        if 'installed' in pkgTypes: 
            instpo = [po for po in self.rpmdb]
            pkgs.extend(instpo)
        if 'available' in pkgTypes:
            # get the Available packages
            availpo = [po for po in self.pkgSack.returnNewestByNameArch() \
                       if not self.simpleDBInstalled(po.name) ] 
            pkgs.extend(availpo)
        if 'updates' in pkgTypes:
            obsoletes = self.up.getObsoletesTuples( newest=1 )
            for ( obsoleting, installed ) in obsoletes:
                obsoleting_pkg = self.getPackageObject( obsoleting )
                installed_pkg =  self.rpmdb.searchPkgTuple( installed )[0]  
                setattr(obsoleting_pkg,'isUpdate',True)                        
                pkgs.append(obsoleting_pkg)
            updates = self.up.getUpdatesList()
            obsoletes = self.up.getObsoletesList()
            for ( n, a, e, v, r ) in updates:
                if ( n, a, e, v, r ) in obsoletes:
                    continue
                matches = self.pkgSack.searchNevra( name=n, arch=a, epoch=e, 
                                                    ver=v, rel=r )
                if len( matches ) > 0:
                    po = matches[0]
                    setattr(po,'isUpdate',True) # Added 'isUpdate' attribute to po                  
                    pkgs.append(po)
                    
                
            
        if sort:
            pkgs.sort(sortPkgObj)
        return pkgs

    def getCategoriesAndGroups(self): 
        '''
        Get the Categories & Groups
        
        Return a list of the following elements
        ([categoryname,categoryid],[(GroupName,GroupId,GroupInstalled),......])
        
        '''       
        catlist = []
        catkeys = [ (c.name,c.categoryid) for c in self.comps.categories]
        catkeys.sort()
        for name,id in catkeys:
            grps = self._getGroupsInCategory(id)
            elem = ([name,id],grps)
            catlist.append(elem)
        return catlist
            
    def _getGroupsInCategory(self,category):
        '''
        Get the Groups in a category
        '''
        for c in self.comps.categories:
            if c.categoryid == category:
                break
        grps = [self.comps.return_group(g) for g in c.groups if self.comps.return_group(g) != None]
        data = [(g.name,g.groupid,g.installed,) for g in grps]
        return data
    
    def getPackagesInGroup(self,grpid,allpkgs=None):
        group = self.comps.return_group(grpid)
        pkgs = []
        pkgs.extend(group.mandatory_packages.keys())
        pkgs.extend(group.default_packages.keys())
        pkgs.extend(group.optional_packages.keys())
        pkgs.extend(group.conditional_packages.keys())
        if not allpkgs:
            allpkgs = self.getPackages()
        grppkgs = []
        for po in allpkgs:
            if po.name in pkgs:
                grppkgs.append(po)
        del pkgs
        del allpkgs
        grppkgs.sort(sortPkgObj)
        return grppkgs        
                
class YumGuiPackageQueue:
    '''
    This class provides a easy interfase to the yum tsInfo.
    
    yumbase = YumGuiBase()
    queue = YumGuiPackageQueue(yumbase)
    ...
    ...
    queue.addPackage(po1)  # Add the po1 yum package object to the queue
    queue.addPackage(po2)  # Add the po2 yum package object to the queue
    ...
    ...
    queue.removePackage(po1) # Remove the po1 yum package object to the queue
    
    '''

    def __init__(self,yumbase,debug=False):
        '''
        @param yumbase: a YumGuiBase object
        '''
        self.yumbase = yumbase
        self.logger = logging.getLogger('yum.verbose.YumGuiPackageQueue')
        self.debug = debug

    def hasPackage(self,po):
        '''
        @param po: Package Object to check for
        '''
        return self.yumbase.tsInfo.exists(po.pkgtup)

    def togglePackage(self,po,isUpdate=False):
        '''
        @param po: Package Object to toggle in queue
        @param isUpdate: The Package Object is an update
        '''
        if self.hasPackage(po):
            self.removePackage(po)
        else:
            self.addPackage(po,isUpdate)
            
        
    def addPackage(self,po,isUpdate=False):
        '''
        @param po: Package Object to add to queue
        @param isUpdate: The Package Object is an update
        '''
        if self.hasPackage(po) == 0:
            if isUpdate:
                txmbr = self.yumbase.update(po) 
                self.logger.info('Added %s for updating' % str(po))
            elif self.yumbase.simpleDBInstalled(po.name,po.arch):
                txmbr = self.yumbase.remove(po)
                self.logger.info('Added %s for removal' % str(po))
            else:
                txmbr = self.yumbase.install(po)
                self.logger.info('Added %s for installation' % str(po))
            if self.debug:
                self.logger.info(self.list())
            return txmbr
        else:
            return None
        
    def removePackage(self,po):        
        '''
        @param po: Package Object to add to queue
        '''
        if self.hasPackage(po):
            self.yumbase.tsInfo.remove(po.pkgtup)
            self.logger.debug('Removed %s ' % str(po))
            if self.debug:
                self.logger.info(self.list())
    
    def getPackages(self):
        '''
        Get all packages in queue
        '''
        return [txmbr.po for txmbr in self.yumbase.tsInfo.getMembers()]
    
    def getMember(self,po):
        '''
        Get a txmbr based on a yum po
        @param po: Yum Package Object. 
        '''
        txmbrs = self.yumbase.tsInfo.getMembers(po.pkgtup)
        if txmbrs:
            return txmbrs[0]
        else:
            return None
        
        
    def clear(self):
        '''
        remove all packages in queue
        '''
        self.yumbase.resetTs()
        
    def list(self):
        """returns a string rep of the  transaction in an easy-to-read way."""
        
        self.yumbase.tsInfo.makelists()
        if len(self.yumbase.tsInfo) > 0:
            out = """
=============================================================================
 %-22s  %-9s  %-15s  %-16s  %-5s
=============================================================================
""" % ('Package', 'Arch', 'Version', 'Repository', 'Size')
        else:
            out = ""

        for (action, pkglist) in [('Installing', self.yumbase.tsInfo.installed),
                            ('Updating', self.yumbase.tsInfo.updated),
                            ('Removing', self.yumbase.tsInfo.removed),
                            ('Installing for dependencies', self.yumbase.tsInfo.depinstalled),
                            ('Updating for dependencies', self.yumbase.tsInfo.depupdated),
                            ('Removing for dependencies', self.yumbase.tsInfo.depremoved)]:
            if pkglist:
                totalmsg = "%s:\n" % action
            for txmbr in pkglist:
                (n,a,e,v,r) = txmbr.pkgtup
                evr = txmbr.po.printVer()
                repoid = txmbr.repoid
                pkgsize = float(txmbr.po.size)
                size = format_number(pkgsize)
                msg = " %-22s  %-9s  %-15s  %-16s  %5s\n" % (n, a,
                              evr, repoid, size)
                for (obspo, relationship) in txmbr.relatedto:
                    if relationship == 'obsoletes':
                        appended = '     replacing  %s.%s %s\n\n' % (obspo.name, obspo.arch, obspo.printVer())
                        msg = msg+appended
                totalmsg = totalmsg + msg
        
            if pkglist:
                out = out + totalmsg

        summary = """
Transaction Summary
=============================================================================
Install  %5.5s Package(s)         
Update   %5.5s Package(s)         
Remove   %5.5s Package(s)         
""" % (len(self.yumbase.tsInfo.installed + self.yumbase.tsInfo.depinstalled),
       len(self.yumbase.tsInfo.updated + self.yumbase.tsInfo.depupdated),
       len(self.yumbase.tsInfo.removed + self.yumbase.tsInfo.depremoved))
        out = out + summary
        
        return out

    def getTransactionList( self ):
        list = []
        sublist = []
        self.yumbase.tsInfo.makelists()        
        for ( action, pkglist ) in [( _( 'Installing' ), self.yumbase.tsInfo.installed ), 
                            ( _( 'Updating' ), self.yumbase.tsInfo.updated ), 
                            ( _( 'Removing' ), self.yumbase.tsInfo.removed ), 
                            ( _( 'Installing for dependencies' ), self.yumbase.tsInfo.depinstalled ), 
                            ( _( 'Updating for dependencies' ), self.yumbase.tsInfo.depupdated ), 
                            ( _( 'Removing for dependencies' ), self.yumbase.tsInfo.depremoved )]:
            for txmbr in pkglist:
                ( n, a, e, v, r ) = txmbr.pkgtup
                evr = txmbr.po.printVer()
                repoid = txmbr.repoid
                pkgsize = float( txmbr.po.size )
                size = format_number( pkgsize )
                alist=[]
                for ( obspo, relationship ) in txmbr.relatedto:
                    if relationship == 'obsoletes':
                        appended = 'replacing  %s.%s %s' % ( obspo.name, 
                            obspo.arch, obspo.printVer() )
                        alist.append( appended )
                el = ( n, a, evr, repoid, size, alist )
                sublist.append( el )
            if pkglist:
                list.append( [action, sublist] )
                sublist = []
        return list        

    def getTransactionSize( self , asFloat=False ):       
        totsize = 0
        for txmbr in self.yumbase.tsInfo.getMembers():
            if txmbr.ts_state in ['i', 'u']:
                po = self.getPackageObject( txmbr.pkgtup )
                if po:
                    size = int( po.size )
                    totsize += size
        if asFloat:
            return  float( totsize )
        else:
            return format_number( float( totsize ) )
        
        