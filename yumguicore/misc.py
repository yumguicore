# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# This file contains classes and funtions perform misc actions
import types

# from output.py (yum)
def format_number(number, SI=0, space=' '):
    """
    Turn numbers into human-readable metric-like numbers
    @param number: The number to format
    @param SI: Use SI units 1K = 1000, else 1K = 1024
    @param space: Space string to put between number and postfix (default = ' ')   
    """
    symbols = ['',  # (none)
                'k', # kilo
                'M', # mega
                'G', # giga
                'T', # tera
                'P', # peta
                'E', # exa
                'Z', # zetta
                'Y'] # yotta

    if SI: step = 1000.0
    else: step = 1024.0

    thresh = 999
    depth = 0

    # we want numbers between 
    while number > thresh:
        depth  = depth + 1
        number = number / step

    # just in case someone needs more than 1000 yottabytes!
    diff = depth - len(symbols) + 1
    if diff > 0:
        depth = depth - diff
        number = number * thresh**depth

    if type(number) == type(1) or type(number) == type(1L):
        format = '%i%s%s'
    elif number < 9.95:
        # must use 9.95 for proper sizing.  For example, 9.99 will be
        # rounded to 10.0 with the .1f format string (which is too long)
        format = '%.1f%s%s'
    else:
        format = '%.0f%s%s'

    return(format % (number, space, symbols[depth]))

def toUTF(txt ):
    '''
    Convert the string to a unicode string
    @param txt:String to convert to unicode 
    '''
    rc=""
    if isinstance(txt,types.UnicodeType):
        return txt
    else:
        try:
            rc = unicode( txt, 'utf-8' )
        except UnicodeDecodeError, e:
            rc = unicode( txt, 'iso-8859-1' )
        return rc


   