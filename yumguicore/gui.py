# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# This file contains classes and funtions perform gui releated actions
import logging
import gtk
import pango
import time

from misc import toUTF


def busyCursor(mainwin,insensitive=False):
    ''' 
    Set busy cursor in mainwin and make it insensitive if selected 
    @param mainwin: The main window glade widget
    @param insensitive: Greyout the main window (default = false)  
    '''
    mainwin.window.set_cursor(gtk.gdk.Cursor(gtk.gdk.WATCH))
    if insensitive:
        mainwin.set_sensitive(False)
    doGtkEvents()

def normalCursor(mainwin):
    ''' 
    Set Normal cursor in mainwin and make it sensitive 
    @param mainwin: The main window glade widget
    '''
    if mainwin.window != None:
        mainwin.window.set_cursor(None)
        mainwin.set_sensitive(True)        
    doGtkEvents()
    
def doGtkEvents():
    ''' Handle pending GTK events '''
    while gtk.events_pending():      # process gtk events
        gtk.main_iteration()
        
def doLoggerSetup(console,logroot,logfmt='%(asctime)s : %(message)s',
                  loglvl=logging.DEBUG):
    '''
    Setup logging in to a TextViewConsole
    @param console: TextViewConsole to log to
    @param logroot: Log root (ex 'yum.verbose')
    @param logfmf: Log Format (default = '%(asctime)s : %(message)s')
    @param loglvl: Log Level (default = logging.DEBUG)
    '''
    logger = logging.getLogger(logroot)
    logger.setLevel(loglvl)
    formatter = logging.Formatter(logfmt, "%H:%M:%S")
    handler = TextViewLogHandler(console)
    handler.setFormatter(formatter)
    handler.propagate = False
    logger.addHandler(handler)
        

class TextViewLogHandler(logging.Handler):
    ''' 
    Python logging handler for writing in a TextViewConsole
    
    Ex.
    import logging
    logHandler = TextViewLogHandler(output) # output is the TextViewConsole to write to
    formatter = logging.Formatter('%(asctime)s : %(message)s', "%H:%M:%S")
    logHandler.setFormatter(formatter)
    logger = logging.getLogger('yum.verbose')
    logger.setLevel(logging.INFO)
    logger.addHandler(logHandler)
    
    '''
    def __init__(self,console,doGTK=True):
        '''
        @param console: The TextViewConsole to write to"
        @param doGTK: Do process gtk event on every call. 
        '''
        logging.Handler.__init__(self)
        self.console = console
        self.doGTK = doGTK
        
    def emit(self,record):   
        msg = self.format(record)
        if self.console:
            if record.levelno < 40:
                self.console.write_line("%s\n" % msg)
            else:
                self.console.write_line("%s\n" % msg,self.console.style_err)  
        if self.doGTK:
            while gtk.events_pending():      # process gtk events
               gtk.main_iteration()    


class TextViewConsole:
    '''
    This class controls a gtk.TextView
    Message can be written in different styles with differnt fonts and colors.
    
    The following styles are predefinded
        self.style_banner (saddle brown, Monospace, 8)
        self.style_ps1 (DarkOrchid4, Monospace,8 ) (Default style)
        self.style_ps2 (DarkOliveGreen, Monospace,8 )
        self.style_out (midnight blue, Monospace,8 )
        self.style_err (red,italic, Monospace,8 )
        
    The colors & fonts can be changend using the 
    changeStyle(color,font,style)
    
    Writing is done using the write_line method
    
    Ex.
    
    obj.write_line("Write This Text")
    
    obj.write_line("Write with the error style",obj.style_err)
        
        
    '''
    def __init__(self,textview,default_style=None,font=None,color=None):
        '''
        @param textview: Glade TextView widget
        @param default_style: Default style to use with write_line
        @param font: Set the ps1 font type
        @param color: Set the ps1 color
        '''
        self.textview = textview
        self.buffer = self.textview.get_buffer()
        self.endMark = self.buffer.create_mark( "End", self.buffer.get_end_iter(), False )
        self.startMark = self.buffer.create_mark( "Start", self.buffer.get_start_iter(), False )
        #setup styles.
        self.style_banner = gtk.TextTag( "banner" )
        self.style_banner.set_property( "foreground", "saddle brown" )
        self.style_banner.set_property( "family", "Monospace" )
        self.style_banner.set_property( "size_points", 8 )
        
            
        self.style_ps1 = gtk.TextTag( "ps1" )
        self.style_ps1.set_property( "editable", False )
        if color:
            self.style_ps1.set_property( "foreground", color )
        else:
            self.style_ps1.set_property( "foreground", "DarkOrchid4" )
        if font:
            self.style_ps1.set_property( "font", font)
        else:
            self.style_ps1.set_property( "family", "Monospace" )
            self.style_ps1.set_property( "size_points", 8 )

        self.style_ps2 = gtk.TextTag( "ps2" )
        self.style_ps2.set_property( "foreground", "DarkOliveGreen" )
        self.style_ps2.set_property( "editable", False )
        self.style_ps2.set_property( "font", "courier" )

        self.style_out = gtk.TextTag( "stdout" )
        self.style_out.set_property( "foreground", "midnight blue" )
        self.style_out.set_property( "family", "Monospace" )
        self.style_out.set_property( "size_points", 8)


        self.style_err = gtk.TextTag( "stderr" ) 
        self.style_err.set_property( "style", pango.STYLE_ITALIC )
        self.style_err.set_property( "foreground", "red" )
        if font:
            self.style_err.set_property( "font", font)
        else:
            self.style_err.set_property( "family", "Monospace" )
            self.style_err.set_property( "size_points", 8 )

        self.buffer.get_tag_table().add( self.style_banner )
        self.buffer.get_tag_table().add( self.style_ps1 )
        self.buffer.get_tag_table().add( self.style_ps2 )
        self.buffer.get_tag_table().add( self.style_out )
        self.buffer.get_tag_table().add( self.style_err )
        
        if default_style:
            self.default_style=default_style
        else:
            self.default_style=self.style_ps1
    
    def changeStyle(self,color,font,style=None):
        '''
        Change the color & font of a style 
        If no style is given, the the default style is changed.
        '''
        if not style:
            self.default_style.set_property( "foreground", color )
            self.default_style.set_property( "font", font )
        else:
            style.set_property( "foreground", color )
            style.set_property( "font", font )
    
    def write_line( self, txt, style=None):
        """ write a line to button of textview and scoll to end
        @param txt: Text to write to textview
        @param style: Style to use. 
        """
        #txt = gobject.markup_escape_text(txt)
        txt = toUTF(txt)
        start, end = self.buffer.get_bounds()
        if style == None:
            self.buffer.insert_with_tags( end, txt, self.default_style )
        else:
            self.buffer.insert_with_tags( end, txt, style )
        self.textview.scroll_to_iter( self.buffer.get_end_iter(), 0.0 )
            

    def clear(self):
        self.buffer.set_text('')
        
    def goTop(self):
        self.textview.scroll_to_iter( self.buffer.get_start_iter(), 0.0 )
        


        
# These classes come from the article
# http://www.linuxjournal.com/article/4702
#
# They have been modified a little to support domain
#       
        
class UI(gtk.glade.XML):
    """Base class for UIs loaded from glade."""
    
    def __init__(self, filename, rootname,domain=None):
        """Initialize a new instance.
        @param filename:  is the name of the .glade file containing the UI hierarchy.
        @param rootname: is the name of the topmost widget to be loaded.
        @param gladeDir: is the name of the directory, relative to the Python
                         path, in which to search for `filename'.
        @param domain: i18n Translation domain for the glade file 
        """
        if domain:
            gtk.glade.XML.__init__(self, filename,root=rootname,domain=domain)
        else:
            gtk.glade.XML.__init__(self, filename, root=rootname)
        self.filename = filename
        self.root = self.get_widget(rootname)

    def __getattr__(self, name):
        """Look up an as-yet undefined attribute, assuming it's a widget."""
        result = self.get_widget(name)
        if result is None:
            raise AttributeError("Can't find widget %s in %s.\n" %
                                 (`name`, `self.filename`))
        
        # Cache the widget to speed up future lookups.  If multiple
        # widgets in a hierarchy have the same name, the lookup
        # behavior is non-deterministic just as for libglade.
        setattr(self, name, result)
        return result

class Controller:
    """
    Base class for all controllers of glade-derived UIs.
    
    Example:
    
    class MyAppl(Controller):
    ''' This class contains all glade signal callbacks '''
    
    
        def __init__( self ):
             # Create and ui object contains the widgets.
             ui = UI( 'path/to/the/gladefile', 'nameofmainwidget', 'translationdomain' )
             # init the Controller Class to connect signals.
             Controller.__init__( self, ui )

        # Add methods for all GTK events defined in the glade.file
        def on_some_widget_event(self,widget,...):
            # The glade widget is available as self.ui.widgetname 
            self.ui.myLabel.set_text('This is some text')
            

    """
    def __init__(self, ui):
        """Initialize a new instance.
        @param ui: is the user interface to be controlled."""
        self.ui = ui
        self.ui.signal_autoconnect(self._getAllMethods())

    def _getAllMethods(self):
        """Get a dictionary of all methods in self's class hierarchy."""
        result = {}

        # Find all callable instance/class attributes.  This will miss
        # attributes which are "interpreted" via __getattr__.  By
        # convention such attributes should be listed in
        # self.__methods__.
        allAttrNames = self.__dict__.keys() + self._getAllClassAttributes()
        for name in allAttrNames:
            value = getattr(self, name)
            if callable(value):
                result[name] = value
        return result

    def _getAllClassAttributes(self):
        """Get a list of all attribute names in self's class hierarchy."""
        nameSet = {}
        for currClass in self._getAllClasses():
            nameSet.update(currClass.__dict__)
        result = nameSet.keys()
        return result

    def _getAllClasses(self):
        """Get all classes in self's heritage."""
        result = [self.__class__]
        i = 0
        while i < len(result):
            currClass = result[i]
            result.extend(list(currClass.__bases__))
            i = i + 1
        return result

class Progress:
    '''
    Simple Progress bar handler.
    '''
    def __init__(self,pbar,label=None,box=None):
        '''
        @param pbar: gtk.ProgressBar widget
        @param label: gtk.Label widget to write info to (Optional)
        @param box: the widget container containing the gtk.ProgressBar & gtk.Label (Optional)
        '''
        self.pbar = pbar
        self.last = 0.0
        self.label = label
        self.box = box
        self.set_markup("")
        self.set_fraction(0.0)

    def show(self):
        if self.box:
            self.box.show()
        doGtkEvents()

    def hide(self):
        if self.box:
            self.box.hide()
        self.reset()    
        doGtkEvents()

    def reset(self):
        self.pbar.set_fraction(0.0)
        self.set_markup("")
        self.pbar.set_text("")
        doGtkEvents()
            

    def set_fraction(self, fract):
        curval = self.pbar.get_fraction()
        if fract > (curval + 0.005) or time.time() > self.last + 0.5:
            self.pbar.set_fraction(fract)
            doGtkEvents()
            self.last = time.time()
        
        

    def get_fraction(self):
        return self.pbar.get_fraction()

    def set_markup(self, txt):
        if self.label:
            self.label.set_markup(txt)
        doGtkEvents()
            

    def set_pbar_text(self, txt):
        self.pbar.set_text(txt)
        doGtkEvents()

        