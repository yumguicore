# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# This file contains different kind of view classes to control gtk.TrewViews

import gtk
import gobject
import os

from i18n import _
from misc import toUTF,format_number

class YumGuiPackageView:
    '''
    A treeview with a list of packages.
    '''
    
    def __init__(self,treeview,queue,transView=None,showActionColumn=True):
        '''
        @param treeview: A Glade Treeview widget
        '''
        self.view = treeview
        # TODO: Add some real content
        self.queue = queue
        self.yumbase = queue.yumbase
        self.model = self.setupView(showActionColumn)
        self.transView= transView
        self.remove_icon = 'edit-delete'        # stock icon to use as action icon for remove
        self.install_icon = 'network-server'    # stock icon to use as action icon for install/update

    def clear(self):
        self.model.clear()    
        
    def setupView(self,showActionColumn):
        model = gtk.ListStore(gobject.TYPE_PYOBJECT,str)
        
        # Setup selection column
        cell = gtk.CellRendererToggle()    # Selection
        cell.set_property( 'activatable', True )
        column = gtk.TreeViewColumn( "", cell )
        column.set_cell_data_func( cell, self._getSelected )
        cell.connect( "toggled", self._onToggled )
        column.set_sort_column_id( -1 )                    
        self.view.append_column( column )        
        
        if showActionColumn:
            # Setup action icon column
            cell2 = gtk.CellRendererPixbuf()    # new
            column2 = gtk.TreeViewColumn( "", cell2 )
            column2.set_cell_data_func( cell2, self._getActionIcon )
            column2.set_sizing( gtk.TREE_VIEW_COLUMN_FIXED )
            column2.set_fixed_width( 20 )
            column2.set_sort_column_id( -1 )            
            self.view.append_column( column2 )

        
        self.view.set_reorderable( False )
        self.view.set_model(model)
        self.view.set_search_column( 1 )
        self.view.set_enable_search(True)
        return model
    
    def createColumn( self, hdr, size,fmtstr,attrs,isMarkup=False):
        """ 
        Create a TreeViewColumn with text and set
        the sorting properties and add it to the view
        """
        cell = gtk.CellRendererText()    
        column = gtk.TreeViewColumn( hdr, cell )
        column.set_resizable( True )
        column.set_cell_data_func( cell, self._getColumnValues,(fmtstr, attrs, isMarkup) )        
        column.set_sizing( gtk.TREE_VIEW_COLUMN_FIXED )
        column.set_fixed_width( size )
        column.set_sort_column_id( -1 )                    
        self.view.append_column( column )        
        return column
        
        
    def _getColumnValues( self, column, cell, model, iter,params ):
        '''
        Column data function, it generates the value based on a format string and
        a list of Yum Package object attribute names to print.
        It supports both text & markup
        '''
        fmtstr,attrs,isMarkup = params
        po = model.get_value( iter, 0 )
        if po:
            values = [self._getPackageAttribute(po, attr) for attr in attrs]
            txt = fmtstr % tuple(values)
            if isMarkup:
                cell.set_property( 'markup', txt )
            else:
                cell.set_property( 'text', txt )

    def _getPackageAttribute(self,po,attr):
        ''' Return yum package object attributes in a way suited for TextView Columns'''
        if hasattr(po,attr):
            value = getattr(po,attr)
            if attr == 'summary':
                desc = getattr(po,attr) or ''
                desc = desc.replace("\n", "")
                if desc:
                    desc = gobject.markup_escape_text(desc)                
                value = toUTF(desc) # Get first line in UTF8
            elif attr == 'size':
                value = format_number(float(getattr(po,attr)))
            return value
        else:
            return None
        
    def _getSelected(self, column, cell, model, iter):
        po = model.get_value( iter, 0 )
        if po.selected:
            cell.set_property( "active", True)
        else:
            cell.set_property( "active", False)

    def _getActionIcon( self, column, cell, model, iter ):
        """ 
        Cell Data function for action icon Column, shows pixmap
        if recent Value is True.
        """
        po = model.get_value( iter, 0 )
        if po:
            # Check if po is in the queue
            if self.queue.hasPackage(po):  
                txmbr = self.queue.getMember(po)
                if txmbr.ts_state in ('i','u'):
                    icon = self.install_icon
                else:
                    icon = self.remove_icon
                cell.set_property( 'visible', True )
                cell.set_property( 'icon-name', icon )
            else:
                cell.set_property( 'visible', False )
            

    def _onToggled(self,widget,path):
        """ Package selection handler """
        iter = self.model.get_iter( path )
        po = self.model.get_value( iter, 0 )
        if not self.queue.hasPackage(po):
            if hasattr(po,'isUpdate'):
                self.queue.addPackage(po,isUpdate=True)
                po.action = "u"
            else:
                self.queue.addPackage(po)                
        else:
            self.queue.removePackage(po)
        po.selected = not po.selected      
        if self.transView: # if a transaction view exist, the refresh it
            self.transView.refresh()      
        
    def populate(self,pkgList,searchAttr=None):
        '''
        Populate the package with a list of package objects
        @param pkgList: List of packages to add to the view
        @param searchAttr: Package attribute to use for typeahead search 
        '''
        self.model.clear()
        for po in pkgList:
            setattr(po,'selected',self._isInstalled(po))
            if searchAttr:
                search = getattr(po,searchAttr)
            else:
                search = ''
            self.model.append([po,search])
        
        
    def _isInstalled(self,po):
        return po.repoid == 'installed'        

            
class YumGuiTransactionView:
    '''
    A Treeview showing the current transaction
    '''        
    def __init__(self,view,queue):
        self.queue = queue
        self.view = view
        self.model = self.setupView()

    def refresh(self):
        pkgs = self.queue.getTransactionList()
        self.populate(pkgs)

    def clear(self):
        self.model.clear()    
        
    def setupView(self):
        model = gtk.TreeStore( gobject.TYPE_STRING, gobject.TYPE_STRING, gobject.TYPE_STRING, gobject.TYPE_STRING, gobject.TYPE_STRING )
        self.view.set_model( model )
        self.createColumn( _( "Name" ), 0 )
        self.createColumn( _( "Arch" ), 1 )
        self.createColumn( _( "Ver" ),  2 )
        self.createColumn( _( "Repository" ), 3 )
        self.createColumn( _( "Size" ),4 )
        return model

    def createColumn( self, hdr, colno, min_width=0 ):
         cell = gtk.CellRendererText()    # Size Column
         column = gtk.TreeViewColumn( hdr, cell, markup=colno )
         column.set_resizable( True )
         if not min_width == 0:
             column.set_min_width( min_width )
         self.view.append_column( column )        
             
             
    def populate( self, pkglist ):
        self.model.clear()       
        for sub, lvl1 in pkglist:
            label = "<b>%s</b>" % sub
            level1 = self.model.append( None, [label, "", "", "", ""] )
            for name, arch, ver, repo, size, replaces in lvl1:
                level2 = self.model.append( level1, [name, arch, ver, repo, size] )
                for r in replaces:
                    level3 = self.model.append( level2, [ r, "", "", "", ""] )
        self.view.expand_all()

        
        
class YumGuiGroupView:
    '''
    A treeview with a list of categories & groups.
    '''
    def __init__( self, view,pkgView,transView,yumbase):
        self.view = view
        self.model = self.setup_view()
        self.pkgView = pkgView
        self.transView = transView
        self.yumbase = yumbase
        self.currentCategory = None
        self.icon_theme = gtk.icon_theme_get_default()
     
    def setup_view( self ):
        """ Setup Group View  """
        model = gtk.TreeStore(gobject.TYPE_BOOLEAN, # Installed
                              gobject.TYPE_STRING,  # Group Name
                              gobject.TYPE_STRING,  # Group Id
                              gobject.TYPE_BOOLEAN, # In queue          
                              gobject.TYPE_BOOLEAN) # isCategory          


        self.view.set_model( model )
        column = gtk.TreeViewColumn(None, None)
        # Selection checkbox
        selection = gtk.CellRendererToggle()    # Selection
        selection.set_property( 'activatable', True )
        column.pack_start(selection, False)
        column.set_cell_data_func( selection, self.setCheckbox )
        selection.connect( "toggled", self.on_toggled )            
        self.view.append_column( column )

        column = gtk.TreeViewColumn(None, None)
        # Queue Status (install/remove group)
        state = gtk.CellRendererPixbuf()    # Queue Status
        state.set_property('stock-size', 1)
        column.pack_start(state, False)
        column.set_cell_data_func( state, self.queue_pixbuf )

        # category/group icons 
        icon = gtk.CellRendererPixbuf()   
        icon.set_property('stock-size', 1)
        column.pack_start(icon, False)
        column.set_cell_data_func( icon, self.grp_pixbuf )
        
        category = gtk.CellRendererText()
        column.pack_start(category, False)
        column.add_attribute(category, 'markup', 1)
        #column2.set_sizing( gtk.TREE_VIEW_COLUMN_FIXED )
        #column2.set_fixed_width( 150 )

        self.view.append_column( column )
        self.view.set_headers_visible(False)
        return model
    
    def setCheckbox( self, column, cell, model, iter ):
        isCategory = model.get_value( iter, 4 )
        state = model.get_value( iter, 0 )
        if isCategory:
            cell.set_property( 'visible', False)
        else:
            cell.set_property( 'visible', True)
            cell.set_property('active',state)
        

    def on_toggled( self, widget, path ):
        """ Group selection handler """
        iter = self.model.get_iter( path )
        grpid = self.model.get_value( iter, 2 )
        inst = self.model.get_value( iter, 0 )
        queued = self.model.get_value( iter, 3 )
        if inst:
            self.yumbase.deselectGroup(grpid)
        else:
            self.yumbase.selectGroup(grpid)
        self.model.set_value( iter, 0, not inst )
        self.model.set_value( iter, 3, not queued)
        self.transView.refresh()
        
    def populate(self):
        self.model.clear()
        data = self.yumbase.getCategoriesAndGroups()        
        for cat,grps in data:
            cName,cId = cat
            node = self.model.append(None,[None,cName,cId,False,True])          
            for grp in grps:
                (gName,gId,gInst) = grp
                self.model.append(node,[gInst,gName,gId,False,False])
            
    def queue_pixbuf( self, column, cell, model, iter ):
        """ 
        Cell Data function for recent Column, shows pixmap
        if recent Value is True.
        """
        grpid = model.get_value( iter, 2 )
        queued = model.get_value( iter, 3 )
        group = self.yumbase.comps.return_group(grpid)
        if group:
            if queued:
                if group.selected:
                    icon = 'network-server'
                else:
                    icon = 'edit-delete'                
                cell.set_property( 'visible', True )
                cell.set_property( 'icon-name', icon )
            else:    
                cell.set_property( 'visible', False)
        else:
           cell.set_property( 'visible', False)

    def grp_pixbuf( self, column, cell, model, iter ):
        """ 
        Cell Data function for recent Column, shows pixmap
        if recent Value is True.
        """
        grpid = model.get_value( iter, 2 )
        pix = None
        fn = "/usr/share/pixmaps/comps/%s.png" % grpid
        if os.access(fn, os.R_OK):
            pix = self._get_pix(fn)
        if pix:
            cell.set_property( 'visible', True )
            cell.set_property( 'pixbuf', pix )
        else:
            cell.set_property( 'visible', False )
            

    def _get_pix(self, fn):
        imgsize = 24
        pix = gtk.gdk.pixbuf_new_from_file(fn)
        if pix.get_height() != imgsize or pix.get_width() != imgsize:
            pix = pix.scale_simple(imgsize, imgsize,
                                   gtk.gdk.INTERP_BILINEAR)
        return pix
        

class YumGuiRepoView:
    '''
    A treeview with a list of available repositories.
    '''
    
    def __init__(self,treeview):
        '''
        @param treeview: A Glade Treeview widget
        '''
        self.view = treeview
        # TODO: Add some real content        
        
        