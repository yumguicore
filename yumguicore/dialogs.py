# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# This file contains misc. dialogs

import gtk

def okDialog(parent, msg):
    '''
    Message dialog with a [OK] button
    @param parent : the main window widget, used to lock the dialog to the
                    main window       
    @param msg: the message to show in dialog
    '''
    dlg = gtk.MessageDialog(parent=parent,
                            type=gtk.MESSAGE_INFO,
                            buttons=gtk.BUTTONS_OK)
    dlg.set_markup(cleanMarkupSting(msg))
    rc = dlg.run()
    dlg.destroy()
    
def questionDialog(parent, msg):
    '''
    Question dialog with [YES]/[NO] buttons
    @param parent : the main window widget, used to lock the dialog to the
                    main window       
    @param msg: the message to show in dialog
    @return: [YES] = True, else False
    '''
    dlg = gtk.MessageDialog(parent=parent,
                            type=gtk.MESSAGE_QUESTION,
                            buttons=gtk.BUTTONS_YES_NO)
    dlg.set_markup(msg)
    rc = dlg.run()
    dlg.destroy()
    if rc == gtk.RESPONSE_YES:
        return True
    else:
        return False    
        
def okCancelDialog(parent, msg):
    '''
    Question dialog with [OK]/[Cancel] buttons
    @param parent : the main window widget, used to lock the dialog to the
                    main window       
    @param msg: the message to show in dialog
    @return: [OK] = True, else False
    '''
    dlg = gtk.MessageDialog(parent=parent,
                            type=gtk.MESSAGE_QUESTION,
                            buttons=gtk.BUTTONS_OK_CANCEL)
    dlg.set_markup(cleanMarkupSting(msg))
    rc = dlg.run()
    dlg.destroy()
    if rc == gtk.RESPONSE_OK:
        return True
    else:
        return False            