# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# This file contains callback handlers


# TODO : This class could be a candidate to include in yum
import time
import logging
import urllib2
import os
import sys

import rpm
from urlgrabber.progress import BaseMeter,format_time
from yum.constants import *
from yum.rpmtrans import RPMBaseCallback


from i18n import _
from gui import doGtkEvents 
from misc import format_number


class DownloadCallback( BaseMeter ):
    """ Customized version of urlgrabber.progress.BaseMeter class """
    def __init__( self):
        BaseMeter.__init__( self )
        self.totSize = ""
               
    def update( self, amount_read, now=None ):
        BaseMeter.update( self, amount_read, now )           

    def _do_start( self, now=None ):
        name = self._getName()        
        self.updateProgress(name,0.0,"","")        
        if not self.size is None:
            self.totSize = format_number( self.size )

    def _do_update( self, amount_read, now=None ):
        fread = format_number( amount_read )
        name = self._getName()        
        if self.size is None:
            # Elabsed time
            etime = self.re.elapsed_time()
            fetime = format_time( etime )
            frac = 0.0
            self.updateProgress(name,frac,fread,fetime)      
        else:
            # Remaining time
            rtime = self.re.remaining_time()
            frtime = format_time( rtime )
            frac = self.re.fraction_read()
            self.updateProgress(name,frac,fread,frtime)      
              

    def _do_end( self, amount_read, now=None ):
        total_time = format_time( self.re.elapsed_time() )
        total_size = format_number( amount_read )
        name = self._getName()        
        self.updateProgress(name,1.0,total_size,total_time)      

    def _getName(self):
        '''
        Get the name of the package being downloaded
        '''
        if self.text and type( self.text ) == type( "" ):
            name = self.text
            print " Text : %s " % name
        else:
            name = self.basename
        return name
        
    def updateProgress(self,name,frac,fread,ftime):
        '''
         Update the progressbar (Overload in child class)
        @param name: filename
        @param frac: Progress fracment (0 -> 1)
        @param fread: formated string containing BytesRead
        @param ftime : formated string containing remaining or elapsed time
        '''
        pass

class YumGuiDownloadCallback(DownloadCallback):
    '''
    Download Progress Callback using the Progress class
    '''
    def __init__(self,progress):
        DownloadCallback.__init__(self)
        self.progress = progress
        self.logger = logging.getLogger( 'yum.verbose.YumGuiDownloadCallback' )   
        
    def updateProgress(self,name,frac,fread,ftime):
        '''
         Update the progressbar (Overload in child class)
        @param name: filename
        @param frac: Progress fracment (0 -> 1)
        @param fread: formated string containing BytesRead
        @param ftime : formated string containing remaining or elapsed time
        '''
        if frac == 0.0 and self.url:
            self.logger.debug( _( "Getting : %s" ) % self.url )
        markup = '<span foreground="blue"> Downloading :</span> %-50s <span foreground="red">%s</span>'
        if not self.size:
              eta = '%5sB %s' % ( fread, ftime )
        else:      
            eta = '%5sB/%5sB %8s ETA' % ( fread, self.totSize, ftime )
        txt = markup % (name,eta)
        self.progress.set_markup(txt)
        self.progress.set_fraction(frac)
        percent  = "%3i%%" % int( frac*100 )
        self.progress.set_pbar_text(percent)
        
    
class YumGuiNoOutputCallback(RPMBaseCallback):
    '''
    yum RPMTransaction display class.
    Do only process gtk events every .5 sek
    '''
    def __init__(self):
        RPMBaseCallback.__init__(self)
        self.last = 0

    def event(self, package, action, te_current, te_total, ts_current, ts_total):
        """
        @param package: is a yum package object or simple string of a package name
        @param action: is a yum.constant transaction set state or in the obscure 
               rpm repackage case it could be the string 'repackaging'
        @param te_current: current number of bytes processed in the transaction
                       element being processed
        @param te_total: total number of bytes in the transaction element being processed
        @param ts_current: number of processes completed in whole transaction
        @param ts_total: total number of processes in the transaction.
        """
        if time.time() > self.last + 0.5:
            doGtkEvents()
            self.last = time.time()

class YumGuiRPMCallback(RPMBaseCallback):
    def __init__(self,progress,markup=None):
        RPMBaseCallback.__init__(self)
        self.progress = progress
        if markup:
            self.markup = markup
        else:
            self.markup = '<span foreground="blue">[%i/%i] %s :</span> %s'
        self.lastname = None

    def event(self, package, action, te_current, te_total, ts_current, ts_total):
        """
        @param package: is a yum package object or simple string of a package name
        @param action: is a yum.constant transaction set state or in the obscure 
               rpm repackage case it could be the string 'repackaging'
        @param te_current: current number of bytes processed in the transaction
                       element being processed
        @param te_total: total number of bytes in the transaction element being processed
        @param ts_current: number of processes completed in whole transaction
        @param ts_total: total number of processes in the transaction.
        """
        process = self.action[action]
        if type(package) not in types.StringTypes:
            pkgname = package.name
        else:
            pkgname = package
                
        if te_total == 0:
            percent = 0
            frac = 0.0
        else:
            frac = float(te_curent)/te_total
            percent = (te_current*100L)/te_total
        
        if pkgname != self.lastname: # new package
            self.progress.set_markup(self.markup % (ts_current,ts_total,process,pkgname))
            self.lastname = pkgname

        self.progress.set_fraction(frac)
        self.progress.set_pbar_text('%i %%' % percent)
                  
class DepSolveCallback:
    """provides text output callback functions for Dependency Solver callback"""
    
    def __init__( self):
        """requires yum-cli log and errorlog functions as arguments"""
        self.logger = logging.getLogger('yum.verbose.DepSolveCallback')
        self.loops = 0
  
            
    def pkgAdded( self, pkgtup, mode ):
        modedict = { 'i': 'installed', 
                     'u': 'updated', 
                     'o': 'obsoleted', 
                     'e': 'erased'}
        ( n, a, e, v, r ) = pkgtup
        modeterm = modedict[mode]
        msg = _( '---> Package %s.%s %s:%s-%s set to be %s' ) % ( n, a, e, v, r, modeterm )
        self.logger.info( msg )
        
    def start( self ):
        self.loops += 1
        
    def tscheck( self ):
        self.logger.info( _( '--> Running transaction check' ) )
        
    def restartLoop( self ):
        self.loops += 1
        self.logger.info( _( '--> Restarting Dependency Resolution with new changes.' ) )
        self.logger.debug( '---> Loop Number: %d' % self.loops )
    
    def end( self ):
        self.logger.info( _( '--> Finished Dependency Resolution' ) )

    
    def procReq( self, name, formatted_req ):
        msg = _( '--> Processing Dependency: %s for package: %s' ) % ( formatted_req, name )
        self.logger.info( msg )
        
    
    def unresolved( self, msg ):
        self.logger.info( _( '--> Unresolved Dependency: %s' ) % msg )

    
    def procConflict( self, name, confname ):
        self.logger.info( _( '--> Processing Conflict: %s conflicts %s' ) % ( name, confname ) )

    def transactionPopulation( self ):
        self.logger.info( _( '--> Populating transaction set with selected packages. Please wait.' ) )
    
    def downloadHeader( self, name ):
        msg = _( '---> Downloading header for %s to pack into transaction set.' ) % name
        self.logger.info( msg )
