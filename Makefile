PKGNAME = yumguicore

clean:
	@rm -f *.pyc *.pyo *~ *.bak *.gladep
	@rm -f yumguicore/*.pyc yumguicore/*.pyo yumguicore/*~ yumguicore/*.bak
	@rm -f tests/*.pyc tests/*.pyo tests/*~ tests/*.bak

