# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

import time
import rpm
import os
import sys
from yumguicore.gui import doGtkEvents
from yum.constants import *

def testProgress(progress):
    ''' 
    test function for the Progress class
    @param progress: instance of the Progress class to test 
    '''
    progress.set_markup('<b>Testing the Progress class</b>')
    for x in range(1001):
        frac = float(x)/1000.0
        percent = int(float(x)/999.0*100)
        progress.set_pbar_text('%i %%' % percent)
        progress.set_fraction(frac)
        time.sleep(.05)
    progress.reset()

def testTextViewConsole(console):
    '''
    Test function for the TextViewConsole class
    @param console: instance of the TextViewConsole class 
    '''
    console.write_line('Testing TextViewConsole class\n')  
    txt = 'Line of text (default style)\n' * 5
    console.write_line(txt)  
    txt = 'Line of text (style_err)\n' * 5
    console.write_line(txt,console.style_err)  
    doGtkEvents()
    
    
def testRPMCallback(callback):
    ''' test the YumGuiRPMCallback class '''
    class fakeTxMember:
        ''' Simulate txmbr '''
        def __init__(self):
            self.output_state = TS_UPDATE
            
    class fakeTsInfo:
        ''' Simulate tsInfo '''
        def __init__(self):
            pass
        
        def getMembers(self,pkgtup):
            return [fakeTxMember()]
            
    def fakeRPMHeader(tup):
        ''' Generate a dictinary to simulate an rpm header '''
        hdr = {}
        hdr['name'], hdr['arch'], hdr['epoch'], hdr['version'], hdr['release'] = tup    
        return hdr
              
    files = [(('foo1','i386','0','1.0','1.fc7'),100000),
             (('foo2','noarch','1','1.1','4.fc7'),200000),
             (('foo3','i386','0','2.0','5.fc7'),300000),
             (('foo4','i386','0','3.0','7.fc7'),400000)]
    if not os.path.exists('/tmp/yumguicore-test'):
        os.mkdir('/tmp/yumguicore-test')
    tsInfo = fakeTsInfo()
    callback.tsInfo = tsInfo
    what = rpm.RPMCALLBACK_TRANS_START    
    callback.callback(what,6,len(files)*2,None,None)
    # Test installing 
    for tup,size in files:
        cursize = 0
        hdr = fakeRPMHeader(tup)
        rpmloc = '/tmp/yumguicore-test/%s-%s-%s.%s.rpm' % (hdr['name'], hdr['version'], hdr['release'],hdr['arch'])
        fd = open(rpmloc,'w') # Create a dummy rpm file
        fd.write(rpmloc)
        fd.close()
        h = (hdr,rpmloc)
        what = rpm.RPMCALLBACK_INST_OPEN_FILE
        callback.callback(what,0,0,h,None)
        what = rpm.RPMCALLBACK_INST_PROGRESS
        while cursize < size:
            cursize += 200
            callback.callback(what,cursize,size,h,None)
        what = rpm.RPMCALLBACK_INST_CLOSE_FILE
        callback.callback(what,0,0,h,None)
        os.remove(rpmloc) # Cleanup the dummy rpm file
    # Test installing 
    for tup,size in files:
        cursize = 0
        hdr = fakeRPMHeader(tup)
        rpmloc = '/tmp/yumguicore-test/%s-%s-%s.%s.rpm' % (hdr['name'], hdr['version'], hdr['release'],hdr['arch'])
        h = (hdr,rpmloc)
        what = rpm.RPMCALLBACK_UNINST_START
        callback.callback(what,0,0,h,None)
        what = rpm.RPMCALLBACK_UNINST_PROGRESS
        while cursize < size:
            cursize += 400
            callback.callback(what,cursize,size,h,None)
        what = rpm.RPMCALLBACK_UNINST_STOP
        callback.callback(what,0,0,h,None)
    callback.reset()
    
    
if __name__ == "__main__":
    pass