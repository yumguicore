# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

import sys
import logging
import gtk
import gtk.glade

import time

import yum.Errors as Errors
import yum.callbacks as callbacks
from yum.callbacks import ProcessTransBaseCallback
from yum.rpmtrans import RPMTransaction


from yumguicore import YumGuiBase,YumGuiPackageQueue, YumGuiDownloadError,\
                       YumGuiDepsolveError,YumGuiDepsolveError, YumGuiTransactionError 
                       
from yumguicore.gui import UI,Controller,TextViewConsole,\
                           TextViewLogHandler,doLoggerSetup,doGtkEvents,Progress
                           
from yumguicore.views import YumGuiPackageView, YumGuiTransactionView,YumGuiGroupView                          
from yumguicore.callback import YumGuiDownloadCallback,YumGuiRPMCallback
from yumguicore.dialogs import questionDialog
import tests

OUTPUT_VIEW = 0
PACKAGE_VIEW = 1
TRANSACTION_VIEW = 2

class MyYumGuiApp(Controller):
    ''' This class contains all glade signal callbacks '''
    
    
    def __init__( self ):
        # Create and ui object contains the widgets.
        ui = UI( 'test.glade' , 'main', 'yumex' )
        # init the Controller Class to connect signals.
        Controller.__init__( self, ui )
        self.logger = logging.getLogger('yum.verbose.MyYumGuiApp')
        self.firstRun = True
        self.packages = None
        self.main() 
         
    #****************** Glade callback methods ********************
    def on_FileQuit(self,widget,data=None):
        '''
        Quit the application
        '''
        gtk.main_quit()       # Exit gtk
        sys.exit(0)
        
    # called if someone clicks the 'Apply' button    
        
    def on_apply_clicked(self,widget):        
        '''
        Run the Transaction, when the apply button is pressed
        '''
        self.ui.book.set_current_page(OUTPUT_VIEW)
        self.processTransaction()
        self.yumbase.reset()
        self.setupYum()

    def on_view_cursor_changed(self,widget): 
        '''
        Show the package description when row is changed
        '''
        self.pkgDesc.clear()               
        ( model, iterator ) = widget.get_selection().get_selected()
        if model != None and iterator != None:
            pkg = model.get_value( iterator, 0 )
            if pkg:
                self.pkgDesc.write_line(pkg.description)


    def on_groupCategories_cursor_changed(self,widget): 
        '''
        Update the package list when the group is changed in the group
        view.
        '''
        ( model, iterator ) = widget.get_selection().get_selected()
        if model != None and iterator != None:
            id = model.get_value( iterator, 2 )
            isCategory = model.get_value( iterator, 4 )
            if not isCategory: # Only update package list if it is a group
                # Get the packages in the current selected group
                pkgs = self.yumbase.getPackagesInGroup(id,self.packages)
                # Populate the group package view
                self.grpPkgView.populate(pkgs)
    
    def on_groupPackages_cursor_changed(self,widget): 
        '''
        '''
        pass
    
    #**************************************************************
    def setupGUI(self):
        ''' Setup the GUI & logging '''
        self.ui.main.connect( "delete_event", self.on_FileQuit )        
        self.ui.main.present()
        self.output = TextViewConsole( self.ui.output)
        self.pkgDesc = TextViewConsole( self.ui.pkgDesc)
        self.progress = Progress(self.ui.progressbar,self.ui.labelProgress,self.ui.vboxProgress)
        doLoggerSetup(self.output,'yum.verbose')
        self.rpmCallback = YumGuiRPMCallback(self.progress)


    def processTransaction(self):    
        '''
        Perform the yum transaction
         - Resolve Dependencies
         - Download Packages.
         - Check GPG Signature.
         - Run Test Transaction
         - Run Transaction
        '''
        try:
            self.progress.show()
            self.logger.info('Resolve Dependencies')
            self.ui.book.set_current_page(OUTPUT_VIEW)
            self.yumbase.doDepSolve()
            self.ui.book.set_current_page(TRANSACTION_VIEW)
            self.transView.refresh()
            self.logger.info(self.queue.list())
            if not questionDialog(self.ui.main,'Do you want to continue with the transaction'):
                return
            self.ui.book.set_current_page(OUTPUT_VIEW)
            self.yumbase.processTransaction(callback=ProcessTransBaseCallback(),
                                            rpmDisplay=self.rpmCallback)
        # Catch depsolve errors
        except YumGuiDepsolveError, msgs:
            mstrs = '\n'.join(msgs)
            msg = _( "Error in Dependency Resolution" )
            self.logger.error(msg)
            self.logger.error(mstrs)
        # catch download errors
        except YumDownloadError, msgs:
            mstrs = '\n'.join(msgs)
            msg = _( "Error in Download" )
            self.logger.error(msg)
            self.logger.error(mstrs)
        # catch Test Transaction errors    
        except YumTestTransactionError, msgs:
            mstrs = '\n'.join(msgs)
            self.logger.error(mstrs)
        # catch other yum errors.
        except YumBaseError, msgs:
            mstrs = '\n'.join(msgs)
            self.logger.error(mstrs)
        self.progress.hide()
    
    def setupYum(self):
        self.progress.show()
        if not self.firstRun: # Dont do reset the first time
            self.yumbase.reset()
        else:
            self.firstRun = False
        self.yumbase.setup(doUpdates=True, doUpdateMetaData=False)
        # Get a list of all packages (installed + available)
        pkgs = self.yumbase.getPackages(sort=True)
        # Get a list of all packages (installed + available)
        updates = self.yumbase.getPackages(pkgTypes=['updates'],sort=True)
        # show the packages in the package view, use po.name for typeahead search
        self.pkgView.populate(pkgs,'name')
        #self.pkgView.populate(updates,'name')
        self.packages = pkgs
        self.pkgDesc.clear()
        self.transView.clear()
        self.grpPkgView.clear()
        self.ui.book.set_current_page(PACKAGE_VIEW)
        self.grpView.populate()
        self.progress.hide()

    def doTests(self):
        '''
        Do some testing. 
        '''
        #tests.testProgress(self.progress)
        #tests.testTextViewConsole(self.output)
        #tests.testRPMCallback(self.rpmCallback)
        #time.sleep(5)
                
    def setupPackagesView(self,view):                
        pkgView = YumGuiPackageView(view,self.queue,self.transView)
        # Add a column to display : name - summary
        pkgView.createColumn('Name',400,'%s - %s',('name','summary'))
        # Add a column to display : epoc:version.release.arch
        pkgView.createColumn('Version',150,'<span foreground="blue">%s:%s.%s.%s</span>',
                                 ('epoch','version','release','arch',),isMarkup = True)
        # Add a column to display : size
        pkgView.createColumn('Size',100,'%s',('size',))
        # Add a column to display : repository
        pkgView.createColumn('Repo',100,'%s',('repoid',))
        return pkgView
         
    def main(self):
        self.setupGUI()
        self.yumbase = YumGuiBase(debuglevel= 4)
        # Setup a PackageQueue an interface to yumbase.tsInfo
        self.queue = YumGuiPackageQueue(self.yumbase,debug=False)
        self.transView = YumGuiTransactionView(self.ui.transaction,self.queue)
        self.grpPkgView = self.setupPackagesView(self.ui.groupPackages)
        self.grpView = YumGuiGroupView(self.ui.groupCategories,self.grpPkgView,self.transView,self.yumbase)
        # Setup a PackageView to display packages.
        self.pkgView = self.setupPackagesView(self.ui.view)
        # Setup Download Progress
        dlCb = YumGuiDownloadCallback(self.progress)
        self.yumbase.setDownloadCallback(dlCb)
        # setup yum (load repo metadata etc)
        self.doTests()
        self.setupYum()
        
        
if __name__ == "__main__":
        mainApp = MyYumGuiApp()
        gtk.main()
        
